#!/bin/bash
docker network create nextcloud
docker start controller || docker run -d --name controller --hostname controller --network nextcloud -v $(pwd):/data -v /var/run/docker.sock:/var/run/docker.sock centos:7 sleep 10h 
docker exec controller yum install -y docker ansible unzip
docker exec controller curl -O https://releases.hashicorp.com/terraform/0.12.9/terraform_0.12.9_linux_amd64.zip
docker exec controller unzip terraform_0.12.9_linux_amd64.zip
docker exec controller mv /terraform /usr/local/bin/
docker exec -it controller  bash 
