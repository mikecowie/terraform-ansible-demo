How to operate:
====

1) Have a linux machine with docker installed and running, and be connected to it over SSH.
2) Check out this code, and run `bash run.sh`. This sets up the controller
3) Now you are in the controller container, which is connected to the docker daemon
4) Scenario1 is a nasty example of terraform and ansible, too tightly entwined. You can operate it with:
    * `cd scenario1`
    * `terraform init`
    * `terraform apply`
5) Make sure you run `terraform destroy` on scenario1 before you switch to scenario2
6) Scenario 2 is an example of the stages being more decoupled and distinct. Everything can be operated with `make`
    * `make test` (should fail)
    * `make plan`
    * `make deploy`
    * `make test` (should still fail)
    * `make configure`
    * `make test` (should now pass)
    
Some disclaimers
====

1) Yes, we're using privileged containers. This is the only way to run systemd containers on docker. No, I don't recommend running systemd as init in containers, except as a test environmenmt for something that will run on VMs.
2) This is not a demo of how to run containers well. In this scenario, the container a simplification of a VM (since it runs a complex service manager) to create a trivial cloud environment that can run on 1CPU and 1GB of RAM.
3) This is meant to be a deployment of the LAMP application nextcloud (multiple app containers with shared storage; a single db container; and a single reverse proxy/loadbalancer container). It's not yet finished, but is enough to demonstrate terraform+ansible working together.
    