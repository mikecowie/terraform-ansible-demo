#!/bin/bash

result=0

#command fail will cause script to exit as a failure if it is ever hit.
function fail {
	result=1
}

#add tests into this function
function tests {
	echo "This test will pass" || fail
	echo "This test will also pass" || fail
	for n in {1..3}; do
		docker container ls | grep web-$n || fail
                docker container ls | grep web-$n || echo "FAIL - no container called web-$n"
        done 	
        for n in 1 ; do
                docker container ls | grep db-$n || fail
                docker container ls | grep db-$n || echo "FAIL - no container called db-$n"
        done
        for n in {1..3}; do
                curl http://web-$n || fail
                curl http://web-$n || echo "FAIL - Can't connect to container web-$n"
        done
}

tests

if [ "$result" == 0 ]; then 
	printf "\n\n all tests passed - success \n"
else
	printf "\n\n at least 1 test failed \n"
fi

exit $result


