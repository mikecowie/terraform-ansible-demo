resource "docker_volume" "share" {
   name = "data"
}

resource "docker_volume" "db" {
   name = "db"
}

resource "docker_container" "lb" {
   count = 1
   name = "lb-${count.index + 1}"
   hostname = "lb-${count.index + 1}"
   image = "centos/systemd"
   privileged = true
   networks_advanced {
     name = "nextcloud"
    }
}

resource "docker_container" "web" {
   count = 3
   name = "web-${count.index + 1}"
   hostname = "web-${count.index + 1}"
   image = "centos/systemd"
   privileged = true
   volumes {
     volume_name = docker_volume.share.name
     container_path = "/data"
     }
   networks_advanced {
     name = "nextcloud"
    }
}

resource "docker_container" "db" {
   count = 1
   name = "db-${count.index + 1}"
   hostname = "db-${count.index + 1}"
   image = "centos/systemd"
   privileged = true
   volumes {
     volume_name = docker_volume.db.name
     container_path = "/data"
     }
   networks_advanced {
     name = "nextcloud"
    }
}
